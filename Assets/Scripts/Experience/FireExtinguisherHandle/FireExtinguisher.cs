using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class FireExtinguisher : MonoBehaviour
{
    [SerializeField] private ParticleSystem fireExtinguisherPS;
    [SerializeField] private GameObject fireExtinguisherPSOrience;
    [SerializeField] private GameObject arrowAnimation;
    [SerializeField] private GameObject safetyPin;
    [SerializeField] private GameObject handleTop;
    [SerializeField] private AudioClip fireExtinguisherAudioClip;
    private RaycastHit hit;
    private Vector3 forward;
    private bool isDropSafetyPin = false;
    private AudioSource fireExtinguisherAudioSources;
    [SerializeField] private bool isSpeaking = false;
    private void Start()
    {
        Init();
    }
    private void Update()
    {
        CheckFireExtinguisherIsHolding();
        ReleaseSafetyPin();
        DrawDirectionRay();
    }
    private void Init()
    {
        SetFirstStateSafetyPin();
        fireExtinguisherAudioSources = GetComponent<AudioSource>();
    }
    private void CheckFireExtinguisherIsHolding()
    {
        if (gameObject.GetComponent<XRGrabInteractable>().isSelected)
        {
            SetStateSafetyPin();
            ChangeStateArrow();
            ChangeStateFireExtinguisher(true);
        }
        else
        {
            ChangeStateFireExtinguisher(false);
        }
    }
    private void SetFirstStateSafetyPin()
    {
        safetyPin.GetComponent<Rigidbody>().isKinematic = true;
        safetyPin.GetComponent<XRGrabInteractable>().enabled = false;
        safetyPin.GetComponent<MeshCollider>().isTrigger = true;
    }
    private void SetStateSafetyPin()
    {
        safetyPin.GetComponent<XRGrabInteractable>().enabled = true;
    }
    private void ReleaseSafetyPin()
    {
        if (Vector3.Distance(handleTop.transform.localPosition, safetyPin.transform.localPosition) >= ExperienceConfig.distanceCanDropPin)
        {
            safetyPin.GetComponent<Rigidbody>().isKinematic = false;
            safetyPin.GetComponent<MeshCollider>().isTrigger = false;
            isDropSafetyPin = true;
        }
        else
        {
            safetyPin.GetComponent<Rigidbody>().isKinematic = true;
            safetyPin.GetComponent<MeshCollider>().isTrigger = true;
            isDropSafetyPin = false;
        }
    }
    private void DrawDirectionRay()
    {
        forward = fireExtinguisherPSOrience.transform.TransformDirection(Vector3.forward * ExperienceConfig.distanceRaycast);
        Debug.DrawRay(fireExtinguisherPSOrience.transform.position, forward, Color.green);
    }
    public void DetectAndPutOutFire()
    {
        if (Physics.Raycast(fireExtinguisherPSOrience.transform.position, forward, out hit, ExperienceConfig.distancePutOutFire))
        {
            if (hit.collider.TryGetComponent(out Fire fire))
            {
                fire.TryExtinguish(ExperienceConfig.amountExtinguishedPerSecond * Time.deltaTime);
            }
        }
    }
    public void ChangeStatePS(bool isPuttingOutFire)
    {
        if (isPuttingOutFire && gameObject.GetComponent<XRGrabInteractable>().isSelected && isDropSafetyPin)
        {
            fireExtinguisherAudioSources.PlayOneShot(fireExtinguisherAudioClip);
            fireExtinguisherAudioSources.Play();
            fireExtinguisherPS.Play();
            DetectAndPutOutFire();
        }
        else
        {
            fireExtinguisherAudioSources.Stop();
            fireExtinguisherAudioSources.Stop();
            fireExtinguisherPS.Stop();
        }
    }
    private void ChangeStateArrow()
    {
        arrowAnimation.gameObject.SetActive(false);
    }
    public void ChangeStateFireExtinguisher(bool isHolding)
    {
        if (isHolding)
        {
            foreach (var colli in transform.GetComponentsInChildren<Collider>())
            {
                if (colli.gameObject.name == ExperienceConfig.nameSafetyPin) continue;
                if (colli != null) colli.isTrigger = true;
            }
            foreach (var rigid in transform.GetComponentsInChildren<Rigidbody>())
            {
                if (rigid.gameObject.name == ExperienceConfig.nameSafetyPin) continue;
                if (rigid != null) rigid.isKinematic = true;
            }
        }
        else
        {
            foreach (var colli in transform.GetComponentsInChildren<Collider>())
            {
                if (colli.gameObject.name == ExperienceConfig.nameSafetyPin) continue;
                if (colli != null) colli.isTrigger = false;
            }
            foreach (var rigid in transform.GetComponentsInChildren<Rigidbody>())
            {
                if (rigid.gameObject.name == ExperienceConfig.nameSafetyPin) continue;
                if (rigid != null) rigid.isKinematic = false;
            }
        }
    }
}
