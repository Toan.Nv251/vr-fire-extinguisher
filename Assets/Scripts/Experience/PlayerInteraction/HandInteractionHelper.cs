using UnityEngine;
using UnityEngine.InputSystem;

public class HandInteractionHelper : MonoBehaviour
{
    [SerializeField] private InputActionProperty gripAnimationAction;
    [SerializeField] private InputActionProperty triggerAnimationAction;
    private Animator handAnimation;
    private void Start()
    {
        Init();
    }
    void Update()
    {
        SetAnimationForHand();
    }
    private void Init()
    {
        handAnimation = GetComponent<Animator>();
    }
    private void SetAnimationForHand()
    {
        float gripValue = gripAnimationAction.action.ReadValue<float>();
        handAnimation.SetFloat("Grip", gripValue);
        float triggerValue = triggerAnimationAction.action.ReadValue<float>();
        handAnimation.SetFloat("Trigger", triggerValue);
    }
}
