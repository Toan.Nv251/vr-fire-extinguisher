using UnityEngine;

public class InteractionHelper : MonoBehaviour
{
    private FireExtinguisher fireExtinguisher;
    public FireExtinguisher FireExtinguisher { get { return fireExtinguisher; } set { fireExtinguisher = value; } }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.GetComponent<FireExtinguisher>())
        {
            fireExtinguisher = other.gameObject.GetComponent<FireExtinguisher>();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<FireExtinguisher>())
        {
            fireExtinguisher = null;
        }
    }
}
