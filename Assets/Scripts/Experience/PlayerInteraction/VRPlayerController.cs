using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class VRPlayerController : MonoBehaviour
{
    [SerializeField] private InteractionHelper[] interactionHelpers;
    [SerializeField] private InputActionProperty leftTriggerAction;
    [SerializeField] private InputActionProperty rightTriggerAction;
    [SerializeField] private XRInteractorLineVisual[] xrInteractionLineViusal;
    private Gradient lineVisualGradient;
    private GradientColorKey[] colorKeys;
    private GradientAlphaKey[] aplhaKeys;
    private void Update()
    {
        CheckTriggerInputAction();
    }
    private void CheckTriggerInputAction()
    {
        if (leftTriggerAction.action.IsPressed() || rightTriggerAction.action.IsPressed())
        {
            foreach(var lineViusal in xrInteractionLineViusal)
            {
                SetColorForLineVisual(1f);
                lineViusal.invalidColorGradient = lineVisualGradient;
            }
            PutOutFire();
        }
        else
        {
            foreach(var lineViusal in xrInteractionLineViusal)
            {
                SetColorForLineVisual(0f);
                lineViusal.invalidColorGradient = lineVisualGradient;
            }
            StopPutOutFire();
        }
    }
    private void PutOutFire()
    {
        foreach(var interactionHelper in interactionHelpers)
        {
            if(interactionHelper.FireExtinguisher != null) interactionHelper.FireExtinguisher.ChangeStatePS(true);
        }
    }
    private void StopPutOutFire()
    {
        foreach (var interactionHelper in interactionHelpers)
        {
            if (interactionHelper.FireExtinguisher != null) interactionHelper.FireExtinguisher.ChangeStatePS(false);
        }
    }
    private void SetColorForLineVisual(float key)
    {
        lineVisualGradient = new Gradient();
        colorKeys = new GradientColorKey[1];
        colorKeys[0].color = Color.white;
        colorKeys[0].time = 0.0f;
        aplhaKeys = new GradientAlphaKey[1];
        aplhaKeys[0].alpha = key;
        aplhaKeys[0].time = 0.0f;
        lineVisualGradient.SetKeys(colorKeys, aplhaKeys);
    }
}
