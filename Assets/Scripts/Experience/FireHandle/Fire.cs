using UnityEngine;
using UnityEngine.Events;

public class Fire : MonoBehaviour
{
    #region Singleton pattern
    public static Fire instance;
    public static Fire Instance
    {
        get
        {
            if(instance == null)
            {
                instance = FindObjectOfType<Fire>();
            }
            return instance;
        }
    }
    #endregion Singleton pattern
    public UnityAction ShowTextDone;
    [SerializeField] private ParticleSystem[] fireParticleSystem = new ParticleSystem[0];
    private AudioSource campFireAudioSources;
    private float currentIntensity = 1f;
    private float[] startIntensities = new float[0];
    private float timeLastWatered = 0;
    private bool isLit = true;
    private void Start()
    {
        Init();
    }
    private void Update()
    {
        RegenFire();
    }
    private void Init()
    {
        SetArrayParticleSystem();
        campFireAudioSources = GetComponent<AudioSource>();
        campFireAudioSources.Play();
    }
    private void SetArrayParticleSystem()
    {
        startIntensities = new float[fireParticleSystem.Length];
        for (int i = 0; i < fireParticleSystem.Length; i++)
        {
            startIntensities[i] = fireParticleSystem[i].emission.rateOverTime.constant;
        }
    }
    private void ChangeIntensity()
    {
        for (int i = 0; i < fireParticleSystem.Length; i++)
        {
            var emission = fireParticleSystem[i].emission;
            emission.rateOverTime = currentIntensity * startIntensities[i];
        }
    }
    public void TryExtinguish(float amount)
    {
        timeLastWatered = Time.time;
        currentIntensity -= amount;
        ChangeIntensity();
        if (currentIntensity <= 0)
        {
            isLit = false;
            campFireAudioSources.Stop();
            ShowTextDone();
        }
    }
    private void RegenFire()
    {
        if (isLit && currentIntensity < 1.0f && Time.time - timeLastWatered >= ExperienceConfig.fireRegenDelay)
        {
            currentIntensity += ExperienceConfig.fireRegenRate * Time.deltaTime;
            ChangeIntensity();
        }
    }
}
