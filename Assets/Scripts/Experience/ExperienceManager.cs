using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ExperienceManager : MonoBehaviour
{
    [SerializeField] private Button choiceButton;
    [SerializeField] private Button resetButton;
    [SerializeField] private TMP_Text contentTextNotice;
    [SerializeField] private TMP_Text titleText;
    [SerializeField] private TMP_Text contentText;
    [SerializeField] private Transform npcS;
    private GameObject uiComponent;
    private Camera vrPlayerCamera;
    private void Start()
    {
        Init();
    }
    private void Update()
    {
        SetRotationCamera();
    }
    private void OnEnable()
    {
        choiceButton.onClick.AddListener(SetTextStepOne);
        resetButton.onClick.AddListener(ResetScene);
    }
    private void OnDisable()
    {
        choiceButton.onClick.RemoveAllListeners();
        resetButton.onClick.RemoveAllListeners();
    }
    private void Init()
    {
        vrPlayerCamera = FindObjectOfType<Camera>();
        uiComponent = GameObject.Find(ExperienceConfig.nameUIComponent);
        SetTextWelcome();
        Fire.Instance.ShowTextDone += SetTextDone;
        resetButton.interactable = false;
    }
    private void SetRotationCamera()
    {
        if (uiComponent != null && vrPlayerCamera != null) uiComponent.transform.rotation = vrPlayerCamera.transform.rotation;
    }
    private void SetTextWelcome()
    {
        titleText.text = ExperienceConfig.textWelcome;
        contentText.text = ExperienceConfig.textContentWelcome;
    }
    private void SetTextStepOne()
    {
        titleText.text = ExperienceConfig.textStepOne;
        contentText.text = ExperienceConfig.textContentStepOne;
        choiceButton.onClick.RemoveAllListeners();
        choiceButton.onClick.AddListener(SetTextStepTwo);
    }
    private void SetTextStepTwo()
    {
        titleText.text = ExperienceConfig.textStepTwo;
        contentText.text = ExperienceConfig.textContentStepTwo;
        choiceButton.onClick.RemoveAllListeners();
        choiceButton.interactable = false;
    }
    private void SetTextDone()
    {
        titleText.text = ExperienceConfig.textDone;
        contentText.text = ExperienceConfig.textContentDone;
        resetButton.interactable = true;
        SetAnimationNPCS();
    }
    private void SetTextNoticeNearFire()
    {
        contentTextNotice.text = ExperienceConfig.textNoticeNearFire;
    }
    private void SetTextNoticeReleasePin()
    {
        contentTextNotice.text = ExperienceConfig.textNoticeDontReleasePin;
    }
    private void ResetScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(ExperienceConfig.nameSceneExperience);
    }
    private void SetAnimationNPCS()
    {
        foreach(var animator in npcS.GetComponentsInChildren<Animator>())
        {
            animator.SetBool("IsLit", false);
        }
    }
}
